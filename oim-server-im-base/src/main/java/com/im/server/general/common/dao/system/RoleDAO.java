package com.im.server.general.common.dao.system;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.im.base.dao.BaseDAO;
import com.im.server.general.common.bean.system.Role;
import com.im.server.general.common.util.UpdateSQLUtil;
import com.onlyxiahui.query.hibernate.QueryWrapper;
import com.onlyxiahui.query.hibernate.util.QueryUtil;

/**
 * 
 * date 2018-07-12 10:32:12<br>
 * description
 * 
 * @author XiaHui<br>
 * @since
 */
@Repository
public class RoleDAO extends BaseDAO {

	String namespace ="role";// Role.class.getName();

	public Role get(String id) {
		return this.readDAO.get(Role.class, id);
	}

	public int updateSelective(Role user) {
		QueryWrapper queryWrapper = QueryUtil.getQueryWrapper(user);
		String sql=UpdateSQLUtil.getUpdateSQL("m_role", "id", queryWrapper);
		return this.executeSQL(sql, queryWrapper);
	}
	
	public void delete(String id) {
		writeDAO.deleteById(Role.class, id);
	}

	public List<Role> queryList(QueryWrapper map) {
		List<Role> list = readDAO.queryPageListByName(namespace + ".queryList", map, Role.class);
		return list;
	}

	public List<Role> getListByOutIds(List<String> outIds) {
		StringBuilder sql = new StringBuilder();

		if (null != outIds && !outIds.isEmpty()) {
			sql.append(" and id not in(");
			int size = outIds.size();
			for (int i = 0; i < size; i++) {
				String id = outIds.get(i);
				sql.append("'");
				sql.append(id);
				sql.append("'");
				if (i < (size - 1)) {
					sql.append(",");
				}
			}
			sql.append(")");
		}
		QueryWrapper map = new QueryWrapper();
		map.put("whereSQL", sql.toString());
		List<Role> list = readDAO.queryListByName(namespace + ".queryList", map, Role.class);
		return list;
	}
}
