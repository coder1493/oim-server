package com.im.server.general.common.dao.system;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.im.base.dao.BaseDAO;
import com.im.server.general.common.bean.system.UserRole;
import com.im.server.general.common.data.system.UserRoleInfo;
import com.onlyxiahui.query.hibernate.QueryWrapper;


/**
 * 
 * date 2018-07-12 11:00:00<br>
 * description
 * 
 * @author XiaHui<br>
 * @since
 */
@Repository
public class UserRoleDAO extends BaseDAO {

	String namespace ="userRole";// UserRole.class.getName();

	

	public void delete(String id) {
		writeDAO.deleteById(UserRole.class, id);
	}

	public List<UserRole> getListByUserId(String userId) {
		QueryWrapper queryWrapper = new QueryWrapper();
		queryWrapper.put("userId", userId);
		List<UserRole> list = readDAO.queryListByName(namespace + ".getListByUserId", queryWrapper, UserRole.class);
		return list;
	}

	public List<UserRole> getListByUserIdList(List<String> userIdList) {
		QueryWrapper map = new QueryWrapper();
		map.put("userIdList", userIdList);
		List<UserRole> list = readDAO.queryListByName(namespace + ".getListByUserIdList", map, UserRole.class);
		return list;
	}

	public void deleteByUserId(String userId) {
		StringBuilder sql = new StringBuilder();
		sql.append("delete from m_user_role where userId='");
		sql.append(userId);
		sql.append("'");
		writeDAO.executeSQL(sql.toString());
	}

	public void deleteByRoleId(String roleId) {

		StringBuilder sql = new StringBuilder();
		sql.append("delete from m_user_role where roleId='");
		sql.append(roleId);
		sql.append("'");
		writeDAO.executeSQL(sql.toString());
	}
	
	public List<UserRoleInfo> getUserRoleInfoListByUserIdList(List<String> userIdList) {
		QueryWrapper map = new QueryWrapper();
		map.put("userIdList", userIdList);
		List<UserRoleInfo> list = readDAO.queryListByName(namespace + ".getUserRoleInfoListByUserIdList", map, UserRoleInfo.class);
		return list;
	}
}
