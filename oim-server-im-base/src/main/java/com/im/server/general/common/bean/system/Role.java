package com.im.server.general.common.bean.system;

import java.util.Date;

import javax.persistence.Entity;

import com.im.base.bean.BaseBean;

/**
 * 
 * @author XiaHui
 * 
 */
@Entity(name = "m_role")
public class Role extends BaseBean {

	private String id;// 角色Id
	private String name;// 角色名字
	private String introduce;// 角色描述
	private Date createTime;// 建立时间
	private String flag;// 有效标志 1：启用 0：停用
	private int grade = 1;// 角色级别0:系统角色1：普通角色
	private int rank;// 排序

	// ////非持久化字段
	public static final String status_disable = "0";
	public static final String status_enable = "1";

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIntroduce() {
		return introduce;
	}

	public void setIntroduce(String introduce) {
		this.introduce = introduce;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public int getGrade() {
		return grade;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

}
