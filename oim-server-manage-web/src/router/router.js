import main from '@/views/main.vue';

export const pageRouter = [
    {
        name: 'login',//登录页面
        path: '/login',
        component: () => import('@/views/login.vue')
    },
    {
        path: '/403',
        meta: {
            title: '403-权限不足'
        },
        name: 'error-403',
        component: () => import('@//views/common/error-page/403.vue')
    },
    {
        path: '/500',
        meta: {
            title: '500-服务端错误'
        },
        name: 'error-500',
        component: () => import('@/views/common/error-page/500.vue')
    },
    {
        path: '/locking',
        name: 'locking',
        component: () => import('@/views/main-components/lockscreen/components/locking-page.vue')
    },
    {
        path: '/*',
        name: 'error-404',
        meta: {
            title: '404-页面不存在'
        },
        component: () => import('@/views/common/error-page/404.vue')
    }
]

// 作为Main组件的子页面展示但是不在左侧菜单显示的路由写在otherRouter里
export const mainRouter = {
    path: '/',
    name: 'mainRouter',
    redirect: '/home',
    component: main,
    children: [
        {
            path: 'home',
            title: {i18n: 'home'},
            name: 'home_index',
            component: () => import('@/views/common/home/home.vue')
        },
        {
            path: 'ownspace',
            title: '个人中心',
            name: 'ownspace_index',
            component: () => import('@/views/common/own-space/own-space.vue')
        }, // 用于展示带参路由
        {
            path: 'message',
            title: '消息中心',
            name: 'message_index',
            component: () => import('@/views/common/message/message.vue')
        }
    ]
};

export const appRouter = [
    {
        name: 'system',
        path: '/system',
        title: '权限管理',
        component: main,
        children: [
            {
                name: 'manage.system.menu.allList',
                path: 'menu',
                title: '菜单管理',
                component: () => import('@/views/system/menu/tree.vue')
            },
            {
                name: 'manage.system.role.list',
                path: 'role',
                title: '角色列表',
                component: () => import('@/views/system/role/role-list.vue')
            },
            {
                name: 'manage.system.admin.list',
                path: 'admin',
                title: '系统用户列表',
                component: () => import('@/views/system/admin/admin-list.vue')
            }
        ]
    },
    {
        name: 'core',
        path: '/core',
        title: '用户数据',
        component: main,
        children: [
            {
                name: 'manage.core.user.list',
                path: 'user',
                title: '用户列表',
                component: () => import('@/views/core/user/user-list.vue')
            },
            {
                name: 'manage.core.group.list',
                path: 'group',
                title: '群列表',
                component: () => import('@/views/core/group/group-list.vue')
            }
        ]
    }
]

export const routers = [
    mainRouter
    ,
    ...appRouter,
    ...pageRouter
];
